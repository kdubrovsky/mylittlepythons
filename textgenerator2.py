
#it makes stupid texts better!

import random, sys

articles = ('The', 'A', 'His', 'Her', 'Another')
nouns = ('cat', 'dog', 'woman', 'man', 'rat', 'girl', 'boy', 'doctor', 'designer', 'programmer')
verbs = ('went', 'sang', 'drank', 'smiled', 'worked', 'cried', 'smoked', 'hurried')
adverbs = ('slowly', 'fast', 'loudly', 'politely')  

try:
    if len(sys.argv)-1:
        n = int(sys.argv[1])
    else:
        n = 5
    while n:
        article = random.choice(articles)+' '
        noun = random.choice(nouns)+' '
        verb = random.choice(verbs)+' '
        
        line = article+noun+verb  
        if random.randint(0, 1):
            adverb = random.choice(adverbs)
            line += adverb                      
        print line
        n -= 1
except ValueError:
    print 'This program works only with numbers'