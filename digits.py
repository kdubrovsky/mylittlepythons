import sys

Zero = ("  ***  ", " *   * ", "*     *", "*     *", "*     *", " *   * ", "  ***  ")
One = (" * ", "** ", " * ", " * ", " * ", " * ", "***")
Two = (" *** ", "*   *", "*  * ", "  *  ", " *   ", "*    ", "*****")
Three = (" *** ", "*   *", "    *", "  ** ", "    *", "*   *", " *** ")
Four = ("   *  ", "  **  ", " * *  ", "*  *  ", "******", "   *  ", "   *  ")
Five = ("*****", "*    ", "*    ", " *** ", "    *", "*   *", " *** ")
Six = (" *** ", "*    ", "*    ", "**** ", "*   *", "*   *", " *** ")
Seven = ("*****", "    *", "   * ", "  *  ", " *   ", "*    ", "*    ")
Eight = (" *** ", "*   *", "*   *", " *** ", "*   *", "*   *", " *** ")
Nine = (" ****", "*   *", "*   *", " ****", "    *", "    *", "    *")

freespace = '     '
Digits = (Zero, One, Two, Three, Four, Five, Six, Seven, Eight, Nine)

dStr = sys.argv[1]

try:
    dInt = int(dStr)
    row = 0
    height = len(Digits[1])
    while row < height:
        line = ""
        for sign in dStr:
            n = int(sign)
            r = Digits[n][row]
            for a in r:
               if a=='*':
                   line += sign
               else:
                   line += ' '
            line += freespace
        print (line)         
        row += 1
except ValueError:
    print ("Please use numbers only")

