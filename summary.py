# Calculate count, sum, minimum, maximum and mean values of entered numbers.         

results = [0, None, None, None]
numbers = []
while True:
    try:
        s = raw_input('Input number or Enter to finish ')
        if not s:
            break
        n = int (s)
        numbers.append(n)
        results[0] += n
        if not results[1] or n < results[1]:
            results[1] = n
        if not results[2] or n > results[2]:
            results[2] = n
    except ValueError:
        print 'Please use numbers or Enter to finish' 
count = len(numbers)
if count:
    results[3] = results[0] / float(count)
    print 'NUMBERS: ', numbers 
    print 'count = ', count , ' sum = ', results[0], ' lowest = ', results[1], ' highest = ', results[2], ' mean = ', results[3]
else:
    print 'You dont enter any number'