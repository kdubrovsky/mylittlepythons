#it makes stupid texts

import random, sys

articles = ('The', 'A', 'His', 'Her', 'Another')
nouns = ('cat', 'dog', 'woman', 'man', 'rat', 'girl', 'boy', 'doctor', 'designer', 'programmer')
verbs = ('went', 'sang', 'drank', 'smiled', 'worked', 'cried', 'smoked', 'hurried')
adverbs = ('slowly', 'fast', 'loudly', 'politely')  

try:
    s = sys.argv[1]
    n = int(s)
except ValueError:
    print 'This program works only with numbers'
    exit()
except IndexError:
    n = 5        #task condition

for i in xrange(n):
    line = random.choice(articles)+' '+random.choice(nouns)+' '+random.choice(verbs)
    if random.randint(0, 1):
        line += ' ' + random.choice(adverbs) 
    print line